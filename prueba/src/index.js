import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import  { Home } from './App';
import * as serviceWorker from './serviceWorker';
import{BrowserRouter as Router,Link, Route } from 'react-router-dom'

//los componentes
//leer url de los apartados de la pagina 

import InscripcionForm from "./components/InscripcionForm"
import TatujesList from "./components/TatuajesList"
import TatuadoresList from "./components/TatuadoresList"
import BlogForm from "./components/BlogForm"
import Prueba from "./components/Test2"


//coneccion a stitch de MongoDB
import { Stitch, AnonymousCredential, RemoteMongoClient } from "mongodb-stitch-browser-sdk";
export const stitch = Stitch.initializeDefaultAppClient('proyecto_tatuajes-kbery');
export const mongo = stitch.getServiceClient(RemoteMongoClient.factory, 'mongodb-atlas')

const routing =(
  <Router>

  <nav className="navbar navbar-inverse">
    <div className="container-fluid">
      <div className="navbar-header">
        <a className="navbar-brand"></a>
      </div>


      <ul id="ubicacion" className="nav navbar-nav">
        <li >
          
          <Link to="/tatuajes"  > Tatuajes</Link>
        </li>
        <li >
          <Link to="/tatuadores" > Tatuadores</Link>
        </li>

        <li >
          <Link to="/blog" > Blog</Link>
        </li>

        <li >
          <Link to={'/inscripcion'} > Inscripcion</Link>
        </li>

        <li >
          <Link to="/prueba" > Prueba</Link>
        </li>

      </ul>
    </div>
  </nav>
  <Route exact path="/" component = {Home} />
  <Route path="/tatuajes" component={TatujesList} />
  <Route path="/tatuadores" component={TatuadoresList} />
  <Route path="/blog" component={BlogForm} />
  <Route path="/inscripcion" component={InscripcionForm} />
  <Route path="/prueba" component={Prueba} />


</Router>


)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
