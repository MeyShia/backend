import React, { Component } from 'react'
import { mongo } from '../index'


class Propietario {
  constructor() {
    this.nombre = ''
    this.apellido = ''
    this.direccion = ''
    this.telefono = ''
    this.dni = ''
    this.email = ''
    // este dato vendra de otra coleccion en mongox
    this.bicicleta = {}
    this.genero = ''

  }
}


export default class PropietarioForm extends Component {
  state = {
    Propietario: new Propietario()
  }
  render() {
    return (
      <div>



        <div className="input-group mb-3">
          <form >

            <div className="">

              <div className="input-group">
                <div className="input-group-prepend">
                  <span className="input-group-text">
                    <i className=" fa fa-user fa-2x " style={{ color: 'blue' }}></i>
                  </span>
                </div>
                <input
                  type="text"
                  aria-label="Nombre"
                  className="form-control"
                  placeholder="Nombre"
                  value={this.state.Propietario.nombre}
                  onChange={(e) => {
                    this.setState(
                      { Propietario: { ...this.state.Propietario, nombre: e.target.value } }
                    )
                  }}

                />

                <input
                  type="text"
                  aria-label="Apellido"
                  className="form-control"
                  placeholder="Apellido"
                  value={this.state.Propietario.apellido}
                  onChange={(e) => {
                    this.setState(
                      { Propietario: { ...this.state.Propietario, apellido: e.target.value } }
                    )
                  }}

                />
                <br></br>

              </div>
              <div className="form-check-inline">
                <input
                  type="radio"
                  className="form-check-input form-control-sm"
                  value="Hombre"
                  checked={this.state.Propietario.genero === "Hombre"}
                  onChange={e => {
                    this.setState({
                      Propietario: { ...this.state.Propietario, genero: e.target.value }
                    })
                  }}
                />
                <label className="form-check-label" style={{ fontSize: 20 }} > Hombre </label>
              </div>
              <div className="form-check-inline">
                <input
                  type="radio"
                  className="form-check-input form-control-sm"
                  value="Mujer"
                  checked={this.state.Propietario.genero === "Mujer"}
                  onChange={e => {
                    this.setState({
                      Propietario: { ...this.state.Propietario, genero: e.target.value }
                    })
                  }}

                />
                <label className="form-check-label" style={{ fontSize: 20 }}> Mujer </label>
              </div>
            </div>
          </form>
        </div>
      </div>


            )
  }
}