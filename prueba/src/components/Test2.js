import React, { Component } from 'react'

export default class Test extends Component {
  state = { nombre: '', precio: 0 }

  grabarNuevo = () => {
    let url = `http://192.168.105.122:3000/clientes`
    fetch( url, {
      method: 'PUT',
      headers: { 
        'Accept': 'application/json', 
        'Content-Type': 'application/json' },
      body: JSON.stringify(this.state)      
    })   
  }
  borrarCliente = () => {
    let url = `http://192.168.105.122:3000/clientes`
    fetch( url, {
      method: 'DELETE',
      headers: { 
        'Accept': 'application/json', 
        'Content-Type': 'application/json' },
      body: JSON.stringify({ id: 55 })      
    })
  }

  render() {
    return (
      <div>
        <input onChange={ e => this.setState({nombre: e.target.value})} />
        <input onChange={ e => this.setState({precio: e.target.value})} />
        <button onClick={ this.borrarCliente }> borrar </button>
        <pre> {JSON.stringify(this.state, undefined, 2)} </pre>
      </div> 
    )
    }
    }
