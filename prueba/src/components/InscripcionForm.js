import React, { Component } from 'react'




class Pedido {
constructor() {
this.fecha = ''
this.producto = []
this.recibido = false
this.comentario = ''
this.radio = ''



}
}

export default class clase extends Component {
state = {
pedido: new Pedido()
}

render() {
return (
<div>
<form>
<input
type="date"
value={this.state.pedido.fecha}
placeholder="fecha"
className="form-control"
onChange={(e) => {
this.setState(
{ pedido: { ...this.state.pedido, fecha: e.target.value } }
)

}}
/>
<br></br>
<p>el input tipo checkbox</p>
<input
type="checkbox"
checked={this.state.pedido.recibido}
placeholder=""
className="form-control"
onChange={(e) => {
this.setState(
{ pedido: { ...this.state.pedido, recibido: !this.state.pedido.recibido } }
)
}}
/>
<br></br>
<p>TextArea</p>

<textarea
className="form-control"
placeholder="deje su comentario"
type="text"
value={this.state.pedido.comentario}
onChange={e => {
this.setState({
pedido: { ...this.state.pedido, comentario: e.target.value }
})
}}

/>
<br></br>
<p>el input tipo radio</p>
<input
type="radio"
className="form-control"
value="pequeño"
checked={this.state.pedido.radio === "pequeño"}
onChange={e => {
this.setState({
pedido: { ...this.state.pedido, radio: e.target.value }
})
}}

/> pequeño
<input
type="radio"
className="form-control"
value="mediano"
checked={this.state.pedido.radio === "mediano"}
onChange={e => {
this.setState({
pedido: { ...this.state.pedido, radio: e.target.value }
})
}}

/> mediano


</form>

<br></br>
<br></br>

<div>
{/*comprobando que se le la url*/}
esto pone en la url : {this.props.match.params.id}
<br></br>
{/* esta linea permite observar en la pantalla como va cambiando el state */}
{JSON.stringify(this.state)}
<div></div>
</div>
</div>
)
}
}